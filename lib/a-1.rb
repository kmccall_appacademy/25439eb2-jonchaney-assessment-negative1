# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  result = []
  (nums[0]..nums[-1]).each do |num|
    if !nums.include?(num)
      result << num
    end
  end
  result
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  base_two = []
  bi_str = binary.to_s
  i = bi_str.length-1
  exp = 0
  while i >= 0
    base_two << bi_str[i].to_i * 2**exp
    exp+=1
    i-=1
  end
  base_two.reduce(:+)
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    selected = {}
    self.each do |k , v|
      if prc.call(k, v)
        selected[k] = v
      end
    end
    selected
  end
end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    merged = {}
    if prc == nil
      self.each do |k, v|
        merged[k] = v
      end
      hash.each do |k,v|
        merged[k] = v
      end
    else
      self.each do |k,v|
        if hash.include?(k)
          merged[k] = prc.call(k,v,hash[k])
        end
      end
      hash.each do |k,v|
        if !self.include?(k)
          merged[k] = v
        end
      end
    end
    merged
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  lucas_pos = [2,1]
  lucas_neg = [1,2]
  if n == 0
    return 2
  elsif n < 0
    (n * -1).times do
      lucas_neg << lucas_neg[-2..-1].reduce(:-)
    end
    return lucas_neg[-1]
  else
    n.times do
      lucas_pos << lucas_pos[-2..-1].reduce(:+)
    end
    return lucas_pos[-2]
  end
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  palindromes = palindromes(string)
  if palindromes.empty?
    false
  elsif palindromes.values[-1] > 2
    palindromes.values[-1]
  else
    false
  end
end

def palindromes(string)
  hash = Hash.new(0)
  i = 0
  while i < string.length
    j = i + 1
    while j < string.length
      sub_str = string[i..j]
      if palindrome?(sub_str)
        hash[sub_str] = sub_str.length
      end
      j+=1
    end
    i+=1
  end
  hash.sort_by {|k, v| v}.to_h
end

def palindrome?(string)
  string == string.reverse
end
